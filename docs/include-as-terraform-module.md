# Method 2: Include as Terraform module

**This method is recommended if you already have a Terraform setup or need additional functionality.**

## Installation

1. Ensure the required prerequisites are installed.

   | executable  | download link                                   |
   | ----------- | ----------------------------------------------- |
   | `terraform` | https://www.terraform.io/downloads              |
   | `kubectl`   | https://kubernetes.io/docs/tasks/tools/#kubectl |

1. Define the Argo CD module in your Terraform setup.

   ```terraform
   ## For provider configuration see https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs.
   provider "kubernetes" {
     # host                   = "https://<your kubernetes api host>"
     # cluster_ca_certificate = "<your ca certificate like '-----BEGIN CERTIFICATE-----...-----END CERTIFICATE-----'>"
     # token                  = "<your kubernetes token>"
   }

   # This can be set to prevent failing an initial plan when Kubernetes and/or its Custom Resource Definitions are not yet available.
   variable "kubernetes_custom_resources_enabled" {
     type    = bool
     default = true
   }

   module "argo_cd" {
     source = "git::https://gitlab.com/vlasman/terraform-argo-cd.git"

     kubernetes_custom_resources_enabled = var.kubernetes_custom_resources_enabled

     ## Optionally provide a new non-existing custom namespace.
     # kubernetes_namespace = "argo-system"

     ## Configuring this block is only necessary if your root app repository is private
     ## and could also be GitHub or something else.
     # git_credentials_url           = "https://gitlab.com/<your username>"
     # git_credentials_read_username = "<your username>"
     # git_credentials_read_token    = "<your api token>"

     ## Connect your root app chart repository. You can fork from https://gitlab.com/vlasman/root-app-chart.
     root_app_repository_url = "<your root app chart repository>"
     root_app_repository_ref = "HEAD"
     ## Optionally provide Helm chart parameters and/or values to pass to your root app
     # root_app_chart_parameters = [
     #   {
     #     name = "baseDomainName"
     #     value = "yourdomain.tld"
     #   }
     # ]
     # root_app_chart_values = <<-EOT
     #   baseDomainName: yourdomain.tld
     # EOT
   }

   output "kubernetes_crds_available" {
     description = "Wether Kubernetes and its Custom Resource Definitions are available"
     value       = module.argo_cd.kubernetes_crds_available
   }

   output "install_completed" {
     description = "Wether the Terraform module install is complete"
     value       = module.argo_cd.install_completed
   }
   ```

1. Provision the module.

   Kubernetes and/or its Custom Resource Definitions are initially not available so first we need to create a plan without the CRD-dependend resources. In the second apply they should be available and all resources can be applied.

   ```sh
   # Only needed for the initial apply
   terraform apply -var=kubernetes_custom_resources_enabled=false
   ```

   ```sh
   terraform apply
   ```

1. If the provisioning is completed test if Argo CD is installed.

   ```sh
   kubectl get pods --all-namespaces
   ```

   You should see a list of pods including the ones from Argo CD.

   ```sh
   kubectl get apps --all-namespaces
   ```

   You should see the 'root' Argo CD application.
