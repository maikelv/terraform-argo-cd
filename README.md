# Terraform Argo CD

This [Terraform](https://www.terraform.io) module installs [Argo CD](https://argoproj.github.io/cd) and creates an Argo CD app which implements the GitOps [app of apps pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/).

**The goal of this module is to setup Argo CD once and from then on maintain your cluster (including Argo CD itself) using Argo CD GitOps pull functionality.**

## Usage

You can use this module in multiple ways.

| Description                                                             | Rationale                                                                  |
| ----------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| Method 1: [Install with new Kubernetes cluster][m1] ([quick-start][ib]) | If you want to get started quickly.                                        |
| Method 2: [Include as Terraform module][m2]                             | If you already have a Terraform setup or need additional functionality.    |
| Method 3: [Apply on an exising Kubernetes cluster][m3]                  | If you already have a running Kubernetes cluster not managed by Terraform. |

[m1]: docs/install-with-new-kubernetes-cluster.md
[m2]: docs/include-as-terraform-module.md
[m3]: docs/apply-on-existing-cluster.md
[ib]: https://gitlab.com/vlasman/infra-bootstrap

## Troubleshooting

- _The Argo CD upgrade failed._

  Delete the Argo CD install job and re-apply using Terraform.

  ```sh
  kubectl delete job argo-cd-install --namespace=argo-system
  terraform apply
  ```

- _Reinstalling Argo CD failed._

  Delete the Argo CD install job, cluster scoped resources and re-apply using Terraform.

  ```sh
  kubectl delete job argo-cd-install --namespace=argo-system
  kubectl delete clusterrole,clusterrolebinding,crd --selector=app.kubernetes.io/part-of=argocd
  terraform apply
  ```

- _Terraform apply failed with REST client error_

  ```
  │ Error: Failed to construct REST client
  │
  │   with kubernetes_manifest.root_app[0],
  │   on main.tf line x, in resource "kubernetes_manifest" "root_app":
  │  x: resource "kubernetes_manifest" "root_app" {
  │
  │ cannot create REST client: no client config
  ```

  The Kubernetes Custom Resource Definitions are initially not available so first we need to create a plan without the CRD-dependend resources. In the second apply they should be available and all resources can be applied.

  ```sh
  # Only needed for the initial apply
  terraform apply -var=kubernetes_custom_resources_enabled=false
  ```

  ```sh
  terraform apply
  ```

## Deletion

To delete Argo CD from your cluster execute the following.

```sh
terraform destroy
kubectl delete clusterrole,clusterrolebinding,crd --selector=app.kubernetes.io/part-of=argocd
```

You may need to delete cluster scoped resources such as CRD's installed by components in your root app.
