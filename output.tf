output "install_completed" {
  description = "Wether the Terraform module install is complete"
  value       = data.kubernetes_resource.job_install.object.metadata.name != null && var.kubernetes_custom_resources_enabled
}

output "kubernetes_crds_available" {
  description = "Wether the Kubernetes Custom Resource Definitions are available"
  # Unfortunatly it is not yet possible to get the status: https://github.com/hashicorp/terraform-provider-kubernetes/issues/1699.
  # The actual job object is only available when it exists in Kubernetes so using that for now.
  value = data.kubernetes_resource.job_install.object.metadata.name != null
}

output "kubernetes_namespace" {
  description = "The Kubernetes namespace Argo CD resources are deployed to"
  value       = var.kubernetes_namespace
}

output "admin_password" {
  description = "The provided/generated Argo CD admin password"
  sensitive   = true
  value       = local.admin_password
}
